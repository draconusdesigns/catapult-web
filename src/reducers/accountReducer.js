import {
    LIST_ACCOUNTS,
    ACCOUNT_LOADING
} from '../actions/types';

const initialState = {
    accounts: null,
    loading: false
};

export default function(state = initialState, action) {
    switch (action.type) {
        case ACCOUNT_LOADING:
            return {
                ...state,
                loading: true
            };
        case LIST_ACCOUNTS:
            return {
                ...state,
                accounts: action.payload,
                loading: false
            };
        default:
            return state;
    }
}
