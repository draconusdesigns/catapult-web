import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';

import { Provider } from 'react-redux';
import store from './store';

import PrivateRoute from './components/common/PrivateRoute';

import Navigation from './components/common/layout/Navigation';
import Footer from './components/common/layout/Footer';

import Login from './components/auth/Login';
import Register from './components/auth/Register';

import Home from './components/home/Home';
import Settings from './components/settings/Settings';

import NotFound from './components/not-found/NotFound';

// Check for token
if (localStorage.jwtToken) {
    // Set auth token header auth
    setAuthToken(localStorage.jwtToken);
    // Decode token and get user info and exp
    const decoded = jwt_decode(localStorage.jwtToken);
    // Set user and isAuthenticated
    store.dispatch(setCurrentUser(decoded));

    // Check for expired token
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        // Logout user
        store.dispatch(logoutUser());
        // Redirect to login
        window.location.href = '/login';
    }
}

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div className="App">
                        <Navigation />
                        <Route exact path="/" component={Login} />
                            <Route exact path="/login" component={Login} />
                            <Route exact path="/register" component={Register} />
                            <Switch>
                                <PrivateRoute exact path="/home" component={Home} />
                                <PrivateRoute exact path="/settings" component={Settings} />
                            </Switch>
                            <Route exact path="/not-found" component={NotFound} />
                        <Footer />
                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;
