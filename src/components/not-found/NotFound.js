import React from 'react';

export default () => {
    return (
        <main id="404" className="container">
            <div className="panel panel-default">
                <div className="panel-body">
                    <h1>Page Not Found</h1>
                    <p>Sorry, this page does not exist</p>
                </div>
            </div>
        </main>
    );
};
