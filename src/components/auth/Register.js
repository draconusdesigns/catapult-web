import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';

class Register extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            username: '',
            firstname: '',
            lastname: '',
            password: '',
            passwordConfirm: '',
            errors: {}
        };
    }

    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
            this.props.history.push('/home');
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push('/home');
        }

        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }

    onSubmit = (event) => {
        event.preventDefault();

        const userData = {
            email: this.state.email,
            username: this.state.username,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            password: this.state.password
        };

        this.props.registerUser(userData);
    }

    onChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    render() {
        const { errors } = this.state;

        return (
                <div className="row justify-content-center text-left">
                    <div className="card col-lg-6 shadow p-3 mb-5 bg-white rounded">
                        <article className="card-body">
                            <Link className="float-right btn btn-outline-info" to="/login">
                                Have an Account?
                            </Link>
                            <h4 className="card-title mb-4 mt-1">
                                Register
                            </h4>
                            <form onSubmit={this.onSubmit}>
                                <TextFieldGroup
                                    label="First Name"
                                    name="firstname"
                                    type="text"
                                    value={this.state.firstname}
                                    onChange={this.onChange}
                                    error={errors.firstname}
                                />
                                <TextFieldGroup
                                    label="Last Name"
                                    name="lastname"
                                    type="text"
                                    value={this.state.lastname}
                                    onChange={this.onChange}
                                    error={errors.lastname}
                                />
                                <TextFieldGroup
                                    label="Email"
                                    name="email"
                                    type="text"
                                    value={this.state.email}
                                    onChange={this.onChange}
                                    error={errors.email}
                                />
                                <TextFieldGroup
                                    label="Username"
                                    name="username"
                                    type="text"
                                    value={this.state.username}
                                    onChange={this.onChange}
                                    error={errors.username}
                                />

                                <TextFieldGroup
                                    label="Password"
                                    name="password"
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.onChange}
                                    error={errors.password}
                                />

                                <div className="form-group">
                                    <input type="submit" className="btn btn-block btn-info" value="Register" />
                                </div>
                            </form>
                        </article>
                    </div>
                </div>
        );
    }
}

Register.propTypes = {
    registerUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, { registerUser })(Register);
