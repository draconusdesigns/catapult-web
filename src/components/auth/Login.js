import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            errors: {}
        };
    }

    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
            this.props.history.push('/home');
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push('/home');
        }

        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }

    onSubmit = (event) => {
        event.preventDefault();

        const userData = {
            username: this.state.username,
            password: this.state.password
        };

        this.props.loginUser(userData);
    }

    onChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    render() {
        const { errors } = this.state;

        return (
                <div className="row justify-content-center text-left">
                    <div className="card col-lg-6 shadow p-3 mb-5 bg-white rounded">
                        <article className="card-body">
                            <Link className="float-right btn btn-outline-info" to="/register">
                                Sign up
                            </Link>
                            <h4 className="card-title mb-4 mt-1">
                                Log In
                            </h4>
                            <form onSubmit={this.onSubmit}>
                                <TextFieldGroup
                                    label="Username"
                                    placeholder="Enter your username"
                                    name="username"
                                    type="text"
                                    value={this.state.username}
                                    onChange={this.onChange}
                                    error={errors.username}
                                />

                                <TextFieldGroup
                                    label="Password"
                                    placeholder="Enter your password"
                                    name="password"
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.onChange}
                                    error={errors.password}
                                />

                                <div className="form-group">
                                    <input type="submit" className="btn btn-block btn-info" value="Log In" />
                                </div>
                            </form>
                        </article>
                    </div>
                </div>
        );
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);
