import React from 'react';

const Home = () => {
    return (
        <main id="home" className="container">
            <div className="row">
                <div className="col-small">
                    <h1>Home</h1>
                </div>
            </div>
        </main>
    );
}

export default Home;
