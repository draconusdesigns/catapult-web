import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './navigation.scss';
import { connect } from 'react-redux';
import { logoutUser } from '../../../actions/authActions';

class Navigation extends Component {
    onLogoutClick(e) {
        e.preventDefault();
        this.props.logoutUser();
    }

    render() {
        const { isAuthenticated, user } = this.props.auth;

        const authLinks = (
            <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link" to="/home">
                            <i className="fas fa-home" aria-hidden="true"></i>
                            Home
                        </Link>
                    </li>
                </ul>
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Hello,&nbsp;
                            { user.username || user.accountType }
                            <span className="caret"></span>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <Link className="dropdown-item" to="/settings">
                                <i className="fas fa-spin fa-cog" aria-hidden="true"></i>
                                Settings
                            </Link>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="/" onClick={this.onLogoutClick.bind(this)}>
                                <i className="fas fa-sign-out-alt" aria-hidden="true"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        );

        const guestLinks = (
            <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav ml-auto">
                </ul>
            </div>
        );

        return (
            <nav className="navbar navbar-expand-lg fixed-top">
                <div className="container">
                    <a className="navbar-brand" href="/#">
                        Catapult
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    {isAuthenticated ? authLinks : guestLinks}
                </div>
            </nav>
        );
    }
}

Navigation.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, { logoutUser })(
    Navigation
);
