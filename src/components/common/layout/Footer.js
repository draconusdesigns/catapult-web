import React from 'react';
import './footer.scss';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container">
                <p className="m-0 text-center">
                    <span className="copyright">
                        &copy; {new Date().getFullYear()} Ryan Holt - <small>Senior Software Engineer</small>
                    </span>
                </p>
            </div>
        </footer>
    );
}

export default Footer;
