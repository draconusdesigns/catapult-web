import React from 'react';

export default () => {
    return (
        <div className="pad-quad text-center">
            <i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <span className="sr-only"></span>
        </div>
    );
};
