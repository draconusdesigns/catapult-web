/* General */
export const GET_ERRORS = 'GET_ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
/* Auth */
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
/* Account */
export const ACCOUNT_LOADING = 'ACCOUNT_LOADING';
export const LIST_ACCOUNTS = 'LIST_ACCOUNTS';
