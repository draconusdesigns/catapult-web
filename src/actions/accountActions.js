import axios from 'axios';
import {
    LIST_ACCOUNTS,
    ACCOUNT_LOADING,
    GET_ERRORS
} from './types';

// Get all accounts
export const listAccounts = () => dispatch => {
    dispatch(setAccountLoading());
    axios
        .get(`/accounts`)
        .then(res =>
            dispatch({
                type: LIST_ACCOUNTS,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: LIST_ACCOUNTS,
                payload: null
            })
        );
};

// Create Account
export const createAccount = (account, history) => dispatch => {
    axios
        .post(`/accounts`, account)
        .then(res => history.push('/dashboard'))
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
};

// Account loading
export const setAccountLoading = () => {
    return {
        type: ACCOUNT_LOADING
    };
};
